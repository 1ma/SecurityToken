<?php

declare(strict_types=1);

namespace UMA\AuthedTokens;

use InvalidArgumentException;

/**
 * The authenticated Token value object.
 *
 * Use this class to generate new authenticated Tokens,
 * to create them from untrusted data and to check their authenticity.
 */
final class Token
{
    /**
     * The hashing algorithm used for the HMAC procedure.
     */
    private const HMAC_HASH = 'sha256';

    /**
     * The length of a raw HMAC-SHA256 hash. Also the token
     * offset at which the authenticated data begins.
     */
    private const HASH_LENGTH = 32;

    /**
     * Holds the raw bytes of the full token (hash + data).
     *
     * @var string
     */
    private $token;

    private function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * Use this factory to issue a brand new authenticated
     * Token from a chunk of arbitrary data and a secret.
     */
    public static function create(string $data, string $secret): Token
    {
        return new self(\hash_hmac(self::HMAC_HASH, $data, $secret, true) . $data);
    }

    /**
     * Use this factory to instantiate Tokens with untrusted
     * base64 data received from the outside world.
     *
     * @throws InvalidArgumentException If $token is not a valid base64 string.
     */
    public static function untrusted(string $token): Token
    {
        // base64_decode() returns garbage when the input is not a
        // valid base64 string. But it shouldn't matter, because
        // garbage cannot ever pass the HMAC verification (I hope).
        //
        // /!\ OH BUT IT DOES BECAUSE base64_decode() IS SHIT /!\
        return new self(StrictBase64::decode($token));
    }

    /**
     * Use this method to verify that the Token is exactly the same
     * you issued. You need the same $secret that you used when the
     * original Token was created.
     */
    public function authentic(string $secret): bool
    {
        return \hash_equals(
            \hash_hmac(self::HMAC_HASH, $this->data(), $secret, true),
            $this->hash()
        );
    }

    /**
     * Cast the Token to string when you need its printable representation.
     */
    public function __toString()
    {
        return StrictBase64::encode($this->token);
    }

    /**
     * Access the raw HMAC hash.
     *
     * If this is a reentrant Token don't trust this method unless
     * you validate the token first with Token::authentic($secret).
     */
    public function hash(): string
    {
        return \substr($this->token, 0, self::HASH_LENGTH);
    }

    /**
     * Access the data portion of the hash.
     *
     * If this is a reentrant Token don't trust this method unless
     * you validate the token first with Token::authentic($secret).
     */
    public function data(): string
    {
        // substr($string, $offset) returns false if
        // $offset runs past the actual string length
        return \strlen($this->token) >= self::HASH_LENGTH ?
            \substr($this->token, self::HASH_LENGTH) : '';
    }
}
