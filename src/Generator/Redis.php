<?php

declare(strict_types=1);

namespace UMA\AuthedTokens\Generator;

use UMA\AuthedTokens\Generator;
use UMA\AuthedTokens\Token;

/**
 * A stateful authenticated token generator that
 * stores a reference to every issued Token in
 * a Redis database.
 *
 * You can pass an optional $ttl argument to specify
 * the Time To Live of the keys in Redis (in seconds).
 * By default they don't expire.
 */
final class Redis implements Generator
{
    /**
     * Length of the keys stored in the Redis server.
     */
    private const REDIS_KEY_LENGTH = 12;

    /**
     * @var \Redis
     */
    private $client;

    /**
     * @var int
     */
    private $ttl;

    public function __construct(\Redis $client, int $ttl = 0)
    {
        $this->client = $client;
        $this->ttl = $ttl;
    }

    public function generate(string $secret): Token
    {
        $key = \random_bytes(self::REDIS_KEY_LENGTH);

        $this->client->set($key, null, $this->ttl);

        return Token::create($key, $secret);
    }
}
