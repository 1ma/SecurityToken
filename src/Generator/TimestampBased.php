<?php

declare(strict_types=1);

namespace UMA\AuthedTokens\Generator;

use Lcobucci\Clock\Clock;
use Lcobucci\Clock\SystemClock;
use UMA\AuthedTokens\Generator;
use UMA\AuthedTokens\Token;

/**
 * A stateless authenticated token generator that
 * issues tokens with the current UNIX timestamp as
 * the data portion.
 *
 * The timestamp is treated as an unsigned integer and
 * packed as 4 bytes.
 *
 * The current implementation of this generator will
 * overflow (and break down) at 2106-02-07 06:28:16 UTC. At
 * this point a UNIX timestamp will no longer fit in 4 bytes.
 */
final class TimestampBased implements Generator
{
    /**
     * @var Clock
     */
    private $clock;

    public function __construct(Clock $clock = null)
    {
        $this->clock = $clock ?? new SystemClock;
    }

    public function generate(string $secret): Token
    {
        return Token::create(\pack('N', $this->clock->now()->getTimestamp()), $secret);
    }
}
