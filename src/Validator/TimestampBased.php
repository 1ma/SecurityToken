<?php

declare(strict_types=1);

namespace UMA\AuthedTokens\Validator;

use Lcobucci\Clock\Clock;
use UMA\AuthedTokens\Token;
use UMA\AuthedTokens\Validator;

/**
 * Companion validator for the TimestampBased generator.
 *
 * After verifying the authenticity of the Token it checks
 * if more than $ttl seconds have passed since the Token
 * was issued. If that's the case, it fails (returns false).
 * Otherwise, it succeeds (returns true).
 */
final class TimestampBased implements Validator
{
    /**
     * @var int
     */
    private $ttl;

    /**
     * @var Clock
     */
    private $clock;

    public function __construct(int $ttl, Clock $clock)
    {
        $this->ttl = $ttl;
        $this->clock = $clock;
    }

    public function validate(Token $token, string $secret): bool
    {
        if (!$token->authentic($secret)) {
            return false;
        }

        return $this->clock->now()->getTimestamp() < self::timestamp($token) + $this->ttl;
    }

    /**
     * Decode the UNIX timestamp from an authenticated Token.
     */
    private static function timestamp(Token $token): int
    {
        return \unpack('N', $token->data())[1];
    }
}
