<?php

declare(strict_types=1);

namespace UMA\AuthedTokens\Validator;

use UMA\AuthedTokens\Token;
use UMA\AuthedTokens\Validator;

/**
 * Companion validator for the Redis generator.
 *
 * After verifying the authenticity of the Token it
 * uses its data portion as a key, and looks it
 * up in the Redis database. If it finds the key,
 * it deletes it and succeeds (returns true).
 * Otherwise it fails (returns false).
 */
final class Redis implements Validator
{
    /**
     * @var \Redis
     */
    private $client;

    public function __construct(\Redis $client)
    {
        $this->client = $client;
    }

    public function validate(Token $token, string $secret): bool
    {
        if (!$token->authentic($secret)) {
            return false;
        }

        if ($this->client->exists($token->data())) {
            $this->client->del($token->data());

            return true;
        }

        return false;
    }
}
