<?php

declare(strict_types=1);

namespace UMA\AuthedTokens;

/**
 * Contract for classes that perform Token
 * validations besides a basic authenticity check.
 *
 * It is implied that each Validator has a companion Generator.
 *
 * Validators need the original $secret to verify the token's
 * authenticity.
 */
interface Validator
{
    public function validate(Token $token, string $secret): bool;
}
