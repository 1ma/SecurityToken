<?php

declare(strict_types=1);

namespace UMA\AuthedTokens;

use InvalidArgumentException;

/**
 * PHP's builtin implementation of base64_decode eats all sorts of
 * invalid garbage without complaining.
 *
 * The default base64_encode is fine but it is also wrapped
 * in here for consistency.
 *
 * @internal
 */
final class StrictBase64
{
    private const BASE64_PATTERN = '@^[+/0-9A-Za-z]+[=]{0,2}$@';

    public static function encode(string $data): string
    {
        return \base64_encode($data);
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function decode(string $data): string
    {
        if (0 !== \strlen($data) % 4) {
            throw new InvalidArgumentException('fuck you');
        }

        if (1 !== \preg_match(self::BASE64_PATTERN, $data)) {
            throw new InvalidArgumentException('aaaand fuck you too');
        }

        return \base64_decode($data, true);
    }
}
