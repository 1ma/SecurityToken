<?php

declare(strict_types=1);

namespace UMA\AuthedTokens;

/**
 * Contract for classes that issue new Tokens.
 *
 * It is implied that each Generator has a companion Validator.
 *
 * Generators need a $secret string to create new Tokens.
 */
interface Generator
{
    public function generate(string $secret): Token;
}
