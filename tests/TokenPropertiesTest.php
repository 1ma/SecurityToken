<?php

declare(strict_types=1);

namespace UMA\AuthedTokens\Tests;

use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use UMA\AuthedTokens\Token;

/**
 * Otjalà algun dia tinguem un property-based testing framework en PHP.
 */
final class TokenPropertiesTest extends TestCase
{
    /**
     * The number of iterations per each property test.
     */
    private const ITERATIONS = 10000;

    public function testABrandNewTokenIsAlwaysAuthentic(): void
    {
        for ($i = 0; $i < self::ITERATIONS; $i++) {
            $secret = self::bytestream(16);
            $data = self::bytestream(16);

            $token = Token::create($data, $secret);

            self::assertTrue($token->authentic($secret), \sprintf(
                'Token unexpectedly failed authenticity check with data 0x%s and secret 0x%s', bin2hex($data), bin2hex($secret))
            );
        }
    }

    public function testAnAuthenticTokenIsAtLeast44BytesLong(): void
    {
        for ($i = 0; $i < self::ITERATIONS; $i++) {
            $secret = self::bytestream(16);
            $data = self::bytestream(16);

            $token = Token::create($data, $secret);

            self::assertGreaterThanOrEqual(44, \strlen((string) $token), \sprintf(
                'Authentic token is unexpectedly short: %s', $token
            ));
        }
    }

    public function testAnUntamperedTokenIsAlwaysAuthentic(): void
    {
        for ($i = 0; $i < self::ITERATIONS; $i++) {
            $secret = self::bytestream(16);
            $data = self::bytestream(16);

            $token = Token::create($data, $secret);

            $reconstructedToken = Token::untrusted((string) $token);

            self::assertTrue($reconstructedToken->authentic($secret), \sprintf(
                'Token unexpectedly failed authenticity check with data 0x%s and secret 0x%s', bin2hex($data), bin2hex($secret))
            );
        }
    }

    public function testGarbageAlwaysThrowsAnInvalidArgumentException(): void
    {
        for ($i = 0; $i < self::ITERATIONS; $i++) {
            $garbage = self::bytestream(128, 64);

            try {
                Token::untrusted($garbage);
            } catch (\Throwable $t) {
                self::assertInstanceOf(\InvalidArgumentException::class, $t);
                continue;
            }

            throw new ExpectationFailedException('An InvalidArgumentException should have been thrown');
        }
    }

    public function testRandomBase64DataNeverPassesValidation(): void
    {
        for ($i = 0; $i < self::ITERATIONS; $i++) {
            $garbageToken = Token::untrusted(self::base64stream(128));

            self::assertFalse($garbageToken->authentic(''));
        }
    }

    public function testTamperedTokenIsNeverAuthentic(): void
    {
        for ($i = 0; $i < self::ITERATIONS; $i++) {
            $secret = self::bytestream(16);
            $data = self::bytestream(16);

            $token = Token::create($data, $secret);

            self::assertTrue(true);
        }
    }

    /**
     * Return a random binary string of length up to $upperBound bytes.
     */
    private static function bytestream(int $upperBound, int $lowerBound = 0): string
    {
        $length = \random_int($lowerBound, $upperBound);

        // random_bytes(0) blows up, so there
        if (0 === $length) {
            return '';
        }

        return \random_bytes($length);
    }

    /**
     * Return a random binary string of length up to $upperBound bytes
     * encoded as base64.
     */
    private static function base64stream(int $upperBound): string
    {
        return \base64_encode(self::bytestream($upperBound, 1));
    }
}
