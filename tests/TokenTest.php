<?php

declare (strict_types=1);

namespace UMA\AuthedTokens\Tests;

use PHPUnit\Framework\TestCase;
use UMA\AuthedTokens\Token;

final class TokenTest extends TestCase
{
    public function testSimpleUsage(): void
    {
        $token = Token::create('foo', 'bar');

        self::assertTrue($token->authentic('bar'));
        self::assertFalse($token->authentic('baz'));

        $reconstructed = Token::untrusted((string) $token);

        self::assertTrue($reconstructed->authentic('bar'));
        self::assertFalse($reconstructed->authentic('baz'));

        self::assertSame('foo', $reconstructed->data());
        self::assertSame((string) $token, (string) $reconstructed);
        self::assertSame('FHkzIYqqvAuLEKKzpcNGhMjZQ0G88QpHNtxycPd0GFFmb28=', (string) $reconstructed);
    }
}
